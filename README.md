# GLAC - GitLab job Artifacts Cleanup utility

Small python program that uses GitLab API to delete job artifacts.
It can work on one or more project ID and/or group ID.

## Installation
There are at least two options:
 - Using the pre-packaged python script glac.pyz, available in the [Releases](https://gitlab.com/alchemy2/glac/-/releases) section;
 - Downloading source code and setting up a virtual environment.

### Pre-packeged script
Go to the latest release and download the compressed script glac.pyz, it is self contained
(non need to install dependencies) and is compatible with python 3.8 and above.
If it is given execution rights it can be run directly.

```bash
./glac.pyz -h
usage: glac.pyz [-h] [-t TOKEN] [-l LEVEL] [-b URL] [-n] [-r] [-p PRJID [PRJID ...]] [-g GRPID [GRPID ...]] [-o DATETIME] [-v]

GitLab job Artifacts Cleanup utility

optional arguments:
  -h, --help            show this help message and exit
  -t TOKEN, --token TOKEN
                        GitLab access token
  -l LEVEL, --log-level LEVEL
                        Logging output level
  -b URL, --baseurl URL
                        GitLab instance base url (default: https://gitlab.com/)
  -n, --dry-run         When specified, artifacts will not be deleted
  -r, --recursive       Process each group recursively
  -p PRJID [PRJID ...], --projects PRJID [PRJID ...]
                        List of project id to process
  -g GRPID [GRPID ...], --groups GRPID [GRPID ...]
                        List of group id to process
  -o DATETIME, --older-than DATETIME
                        Delete only artifacts older than the specified date time in any valid ISO 8601 format (default:
                        2024-01-24 15:19:27.591710+01:00)
  -v, --version         Print program version and exit

Options -p and -g accepts multiple arguments. The token to access GitLab APIs can also be specified through the GLAC_TOKEN
environment variable.
```

### Setting up a virtual environment
```bash
cd glac
python -m venv venv
./venv/bin/pip install -r requirements.txt
./venv/bin/python glac.py 
```
