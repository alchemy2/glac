#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is a small python program to clear up old gitlab build artifacts.

Inspired by https://gist.github.com/smashnet/4581a1e6dc4af5ae10dfa1296f276bec
"""

__author__ = "Filippo Veneri"
__copyright__ = "Copyright © 2024, Filippo Veneri"
__email__ =  "filippo.veneri@gmail.com"
__license__ = "MIT"
__version__ = "0.0.0+dev"


import datetime
import functools
import json
import logging
import os.path
import sys
import argparse
import urllib.parse
import time
import pytz
import requests


def localize_datetime(dt: datetime.datetime) -> datetime.datetime:
    # check if dt is tzware first
    if dt.tzinfo is not None and dt.tzinfo.utcoffset(dt) is not None:
        return dt
    localtime = time.localtime()
    tz = pytz.timezone(localtime.tm_zone)
    return dt.replace(tzinfo=tz)


def dt_fromisoformat(dtstr: str) -> datetime.datetime:
    dt = datetime.datetime.fromisoformat(dtstr)
    return localize_datetime(dt)


def cleanup_artifacts(args: argparse.Namespace) -> int:

    api = GitlabAPI(args.baseurl, args.token)

    jobs = []

    if args.projects is not None:
        for pid in args.projects:
            jobs.extend(filter(lambda job: (job['date'] < args.older_than) if ('date' in job and job['date'] is not None) else False, api.fetch_project_jobs(pid)))
    
    if args.groups is not None:
        for gid in args.groups:
            projects = api.fetch_group_projects(gid, args.recursive)
            for prj in projects:
                jobs.extend(filter(lambda job: (job['date'] < args.older_than) if ('date' in job and job['date'] is not None) else False, api.fetch_project_jobs(prj['id'])))

    total_size = functools.reduce(
        lambda total, job: total + (
            functools.reduce(lambda sub_total, artifact: sub_total + artifact['size'] if artifact['size'] else 0, job['artifacts'], 0)),
            jobs, 0)
    
    logging.info(f"about to delete artifacts for a total of {format_bytes(total_size)}")
    deleted_bytes = 0
    for job in jobs:
        if not args.dry_run:
            deleted_bytes += api.delete_job_artifacts(job)

    if args.dry_run:
        logging.info('just kidding, it was just a dry run')
    else:
        logging.info(f"deleted a total of {format_bytes(deleted_bytes)}")
    return deleted_bytes


# got it from https://stackoverflow.com/questions/12523586/python-format-size-application-converting-b-to-kb-mb-gb-tb
def format_bytes(bytes_to_format):
    """Return the given bytes as a human friendly kb, mb, gb, or tb string."""
    b = float(bytes_to_format)
    kb = float(1024)
    mb = float(kb ** 2)  # 1,048,576
    gb = float(kb ** 3)  # 1,073,741,824
    tb = float(kb ** 4)  # 1,099,511,627,776

    if b < kb:
        return '{0} {1}'.format(b, 'Bytes' if 0 == b > 1 else 'Byte')
    elif kb <= b < mb:
        return '{0:.2f} KB'.format(b / kb)
    elif mb <= b < gb:
        return '{0:.2f} MB'.format(b / mb)
    elif gb <= b < tb:
        return '{0:.2f} GB'.format(b / gb)
    elif tb <= b:
        return '{0:.2f} TB'.format(b / tb)


class GitlabAPI:
    def __init__(self, base_url: str, token: str):
        self.base_url = urllib.parse.urljoin(base_url, '/api/v4/')
        self.token = token
        self.headers = {'PRIVATE-TOKEN': token}
    
    def call_api(self, method: str, url: str) -> list:
        results = []
        while url is not None:
            logging.debug(f'{method} request to {url}')

            if method == 'get':
                result = requests.get(url, headers=self.headers)
            elif method == 'delete':
                result = requests.delete(url, headers=self.headers)
            else:
                raise RuntimeError(f"unsupported method '{method}'")

            if result.status_code >= 400:
                raise RuntimeError(f'API call failed! {method} {url} returned {result.status_code}')

            results.append(result.content)
            #logging.debug(result.links)
            url = result.links['next']['url'] if 'next' in result.links else None

        return results

    def fetch_project_jobs(self, pid) -> list:
        date_format = "%Y-%m-%dT%H:%M:%S.%f%z"

        url = urllib.parse.urljoin(self.base_url, f'projects/{pid}/jobs?per_page=2000')

        job_list = []
        try:
            for jobs_batch in self.call_api('get', url):
                jobs = json.loads(jobs_batch)
                for j in jobs:
                    if j['finished_at'] is not None:
                        date = datetime.datetime.strptime(j['finished_at'], date_format)
                    else:
                        date = None
                    artifacts_size = functools.reduce(
                        lambda total, artifact: total + artifact['size'] if artifact['size'] else 0, j['artifacts'], 0
                    )
                    job_list.append({
                        'id': j['id'],
                        'project_id': pid,
                        'artifacts': j['artifacts'],
                        'artifacts_size': artifacts_size,
                        'date': date
                    })
        except Exception as ex:
            logging.error(ex)

        return job_list
    
    def fetch_group_projects(self, gid: str, recursive: bool) -> list:
        logging.debug(f'start fetching group {gid} projects')

        grp_to_process = [gid]
        list_of_projects = []

        while len(grp_to_process) > 0:
            grp_id = grp_to_process.pop()
            # if recursive, add subgroups to the stack
            if recursive:
                for subgroups_batch in self.call_api('get', urllib.parse.urljoin(self.base_url, f'groups/{grp_id}/subgroups?per_page=100')):
                    subgroups_list = json.loads(subgroups_batch)
                    for sub in subgroups_list:
                        grp_to_process.append(sub['id'])
            
            #get this group projects
            for project_batch in self.call_api('get', urllib.parse.urljoin(self.base_url, f'groups/{grp_id}/projects?simple=true&archived=false&per_page=100')):
                projects_list = json.loads(project_batch)
                for p in projects_list:
                    list_of_projects.append({
                        'gid': grp_id,
                        'id': p['id'],
                        'name': p['path_with_namespace'],
                    })

        return list_of_projects
    
    def delete_job_artifacts(self, job: dict) -> int:
        if len(job['artifacts']) == 0:
            return 0
            
        if job['date'] == None:
            return 0
        
        freed_bytes = functools.reduce(
            lambda total, artifact: total + artifact['size'] if artifact['size'] else 0, job['artifacts'], 0
        )

        try:
            url = urllib.parse.urljoin(self.base_url, f'projects/{job["project_id"]}/jobs/{job["id"]}/artifacts')
            self.call_api('delete', url)
        except RuntimeError:
            pass

        logging.info(f"deleted {format_bytes(freed_bytes)} for job {job['id']}")
        return freed_bytes


class DebugLevelAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs not allowed")
        super().__init__(option_strings, dest, **kwargs)
    def __call__(self, parser, namespace, values, option_string=None):
        #nameToLevel = logging.getLevelNamesMapping()
        name = values[0]
        level = logging.NOTSET
        if name in logging._nameToLevel:
            level = logging._nameToLevel[name]
        setattr(namespace, self.dest, level)


def main():
    parser = argparse.ArgumentParser(
        prog=sys.argv[0],
        description="GitLab job Artifacts Cleanup utility",
        epilog='Options -p and -g accepts multiple arguments. The token to access GitLab APIs can also be specified through the GLAC_TOKEN environment variable.'
    )
    
    parser.add_argument('-t', '--token', default=os.environ.get('GLAC_TOKEN'), help='GitLab access token')
    parser.add_argument('-l', '--log-level', action=DebugLevelAction, choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'], default='INFO', help='Logging output level', metavar='LEVEL')
    parser.add_argument('-b', '--baseurl', default='https://gitlab.com/', help='GitLab instance base url (default: %(default)s)', metavar='URL')
    parser.add_argument('-n', '--dry-run', action='store_true', help='When specified, artifacts will not be deleted')
    parser.add_argument('-r', '--recursive', action='store_false', help='Process each group recursively')
    parser.add_argument('-p', '--projects', nargs='+', help='List of project id to process', metavar='PRJID')
    parser.add_argument('-g', '--groups', nargs='+', help='List of group id to process', metavar='GRPID')
    parser.add_argument('-o', '--older-than',
                        default=localize_datetime(datetime.datetime.now()),
                        type=dt_fromisoformat,
                        help='Delete only artifacts older than the specified date time in any valid ISO 8601 format (default: %(default)s)',
                        metavar='DATETIME')
    parser.add_argument('-v', '--version', action='store_true', help='Print program version and exit')

    args = parser.parse_args()
    
    logging.basicConfig(level=args.log_level)
    logging.debug(args)

    if args.version:
        print(__version__)
        sys.exit(0)

    if args.token is None:
        logging.error("please specify a an API token to proceed")
        sys.exit(-1)

    if args.projects is None and args.groups is None:
        logging.error("no projects to process, please specify at least one with options -p or -g")
        sys.exit(-1)

    cleanup_artifacts(args)


if __name__ == '__main__':
    main()
